
# Working with the ratiometric estimator


## Getting the standard error of the ratiometric estimator

Given measurements at two exitation wavelengths (340 and 380 nm), \(\widehat{ADU}_{340}\) and \(\widehat{ADU}_{380}\) (these are pooled photon counts over the \(P\) pixels of the ROI) and concomitant measurements \(\widehat{ADU}_{B,340}\) and \(\widehat{ADU}_{B,380}\) (pooled photon counts over the \(P_B\) pixels of the "background region"), what we will refer to as the  "classical" ratiometric estimator at time \(t_i\), is ([Joucla et al (2010), Eq. 8, p. 1133](http://jn.physiology.org/content/103/2/1130)):

\begin{equation}
\widehat{Ca}(t_i) = K_{eff} \, \frac{\hat{r}(t_i)-R_{min}}{R_{max}-\hat{r}(t_i)} \, , \label{eq:RatiometricEstimator}
\end{equation}

where \(K_{eff}\), \(R_{min}\) and \(R_{max}\) are calibrated parameters (assumed exactly known for now) and where [Joucla et al (2010), Eq. 6, p. 1133](http://jn.physiology.org/content/103/2/1130):

\begin{equation}
\hat{r}(t_i) = \frac{\left(T_{e,340}\right)^{-1}\, (\widehat{ADU}_{340}/P - \widehat{ADU}_{B,340}/P_B)}{\left(T_{e,380}\right)^{-1}\, (\widehat{ADU}_{380}/P - \widehat{ADU}_{B,380}/P_B)} \, , \label{eq:ADUratio}
\end{equation}

where \(T_{e,\lambda}\) is the flash duration at wavelength \(\lambda\). Our model for the fluorescence intensity at each wavelength is  [Joucla et al (2010), Eq. 2a and 2b](http://jn.physiology.org/content/103/2/1130):

\begin{equation}
F_{340} =  \left\{\frac{[Fura]_{total}\, \phi}{K_{Fura}+[Ca^{2+}]}\left(R_{min}\, K_{eff} + R_{max} [Ca^{2+}]\right) + s_{B,340}\right\} \, T_{e,340} \, P \, , \label{eq:F340}
\end{equation}

and

\begin{equation}
F_{380} =  \left\{\frac{[Fura]_{total}\, \phi}{K_{Fura}+[Ca^{2+}]}\left(K_{eff} + [Ca^{2+}]\right) + s_{B,380}\right\} \, T_{e,380} \, P  \, , \label{eq:F380}
\end{equation}

where \(s_{B,\lambda}\) is the auto-fluorescence at wavelength
\(\lambda\) &#x2013;assumed homogeneous among the \(P\) pixels of the ROI&#x2013;,
\(K_{Fura}\) is a calibrated parameter,
\([Fura]_{total}\, \phi\), is the total (bound plus free) concentration of
Fura in the cell multiplied by a dimensionless experiment specific
parameter, \(\phi\), lumping together the quantum efficiency, the neurite
thickness, etc.

Under our assumptions ([Joucla et al (2010), Eq. 5, p. 1132](http://jn.physiology.org/content/103/2/1130)) we have (we don't use a "\(\widehat{\quad}\)" here since we are dealing with a *random variable* not a *realization* of it):

\begin{equation}
ADU_{\lambda}(t_i) = G\, F_{\lambda}(t_i) + \epsilon \, G\, \sqrt{F_{\lambda}(t_i) + \sigma^2_{read-out}}  \, , \label{eq:ADUdist}
\end{equation}

where \(G\) is the camera gain, \(\sigma^2_{read-out}\) is its read-out variance, \(F_{\lambda}(t_i)\) is given by Eq. \ref{eq:F340} and \ref{eq:F380} and where \(\epsilon \sim \mathcal{N}(0,1)\) (\(\epsilon\) is a Gaussian random variable with mean 0 and variance 1). In words: \(ADU_{\lambda}(t_i)\) has a Gaussian distribution with mean \(G\, F_{\lambda}(t_i)\) and variance \(G^2\, \left(F_{\lambda}(t_i) + \sigma^2_{read-out}\right)\).

So, to have the variance of \(ADU_{\lambda}(t_i)\) we need to know \(F_{\lambda}(t_i)\) and for that we need to know \([Ca^{2+}](t_i)}\) precisely what we want to estimate&#x2026; But \(\mathrm{E}ADU_{\lambda}(t_i)\),  the expected value of \(ADU_{\lambda}(t_i)\), is \(G\, F_{\lambda}(t_i)\) so we can use the observed value \(\widehat{ADU}_{\lambda}(t_i)\) of \(ADU_{\lambda}(t_i)\) as a guess for \(G\, F_{\lambda}(t_i)\) &#x2013;in Eq. \ref{eq:ADUdist} we plug-in \(\widehat{ADU}_{\lambda}(t_i)\) for \(G\, F_{\lambda}(t_i)\) &#x2013;  leading to:

\begin{equation}
\hat{\sigma}^2_{ADU_{\lambda}(t_i)} = G\, \widehat{ADU}_{\lambda}(t_i) + G^2\, \sigma^2_{read-out} \approx \sigma^2_{ADU_{\lambda}(t_i)} \, . \label{eq:ADUapproxVar}
\end{equation}  

In practice, when we are dealing with several pixels making a ROI, we must multiply the read-out variance by the number of pixels used. Now that we have a \(\hat{\sigma}^2_{ADU_{\lambda}(t_i)}\) we can work with, we want to get \(\hat{\sigma}^2_{r(t_i)}\) and \(\hat{\sigma}^2_{\widehat{Ca}(t_i)}\). We could use the [propagation of uncertainty](https://en.wikipedia.org/wiki/Propagation_of_uncertainty) (or error propagation) together with Eq. \ref{eq:ADUratio} and \ref{eq:RatiometricEstimator} for that, but we can also use a "quick" Monte Carlo approach: we draw a thousand quadruple of vectors \(\left(ADU_{340}^{[j]}(t_i),ADU_{380}^{[j]}(t_i),ADU_{B,340}^{[j]}(t_i),ADU_{B,380}^{[j]}(t_i)\right)\) (\(j=1,\ldots,1000\)) from four independent Gaussian distributions of the general form:  

\begin{equation}\label{eq:ADUapproxVarMC}
ADU_{\lambda}^{[j]}(t_i) = ADU_{\lambda}(t_i) + \epsilon_{j} \, \hat{\sigma}_{ADU_{\lambda}(t_i)}\, ,
\end{equation}

plug-in these quadruples into Eq. \ref{eq:ADUratio} giving us 1000 \(r^{[j]}(t_i)\) before plugging in the latter into Eq. \ref{eq:RatiometricEstimator} leading to 1000 \(\widehat{Ca}^{[j]}(t_i)\). The empirical variance of these simulated observations will be used as \(\hat{\sigma}^2_{\widehat{Ca}(t_i)}\).

The method proposed here is slightly less rigorous than the "direct approach" of Joucla et al (2010) but it is far more flexible since it does not require an independent estimation / measurement of \([Fura]_{total}\). In the present study we also chose to consider the calibrated parameters \(K_{eff}\), \(R_{min}\) and \(R_{max}\) as known. Of course only an estimation (necessarily imprecise since the calibration procedure is subject to errors) is known. But the same batch of Fura was used for all experiments that then should all have the same (systematic) error. We are not trying to get the exact standard error of the calcium dynamics parameters but to show difference between two protocols ("classical whole-cell" versus beta-escin perforated whole-cell), ignoring the uncertainty on the calibrated parameters makes our estimates less variable and helps making the difference, if it exists, clearer. For a true calcium buffering capacity study, the errors on the calibrated parameters should be accounted for and it would be straightforward to do it with our approach, simply by drawing also \(K_{eff}\), \(R_{min}\) and \(R_{max}\) values from Gaussian distributions centered on their respective estimates with a SD given by the standard errors.


## Objective or "cost" function

We know now how to get both sequences of \([Ca^{2+}]\) estimates (Eq. \ref{eq:RatiometricEstimator}) and standard errors from sequences of \(ADU\) measurements. We also entertain a calcium dynamics model (Neher and Augustine, 1992, Eq. 8-10, p. 278):

\begin{equation}\label{eq:FullModel}
\frac{\mathrm{d}Ca(t)}{\mathrm{dt}} \left(1 + \kappa_{F}(Ca(t)) + \kappa_{E}(Ca(t)) \right) + \frac{j(Ca(t))}{v} = 0 \, , 
\end{equation}

where \(Ca(t)\) stands for \([Ca^{2+}]_{free}\) at time t, \(v\) is the volume of the neurite&#x2014;within which diffusion effects can be neglected&#x2014;and

\begin{equation}\label{eq:Extrusion}
j(Ca(t)) \equiv \gamma (Ca(t) - Ca_{steady}) \, ,
\end{equation}

is the model of calcium extrusion---\(Ca_{steady}\) is the steady state \([Ca^{2+}]_{free}\) &#x2014; and:

\begin{equation}\label{eq:kappa}
\[\kappa_{F}(Ca(t)) \equiv \frac{F_{total} \, K_{F}}{(K_{F} + Ca(t))^2} \quad \mathrm{and} \quad \kappa_{E}(Ca(t)) \equiv \frac{E_{total} \, K_{E}}{(K_{E} + Ca(t))^2} \, ,
\end{equation}

where \(F\) stands for the fluorophore en \(E\) for the *endogenous* buffer. 

If we now know (or guess) the values of \(\theta \equiv \left(Ca_{steady}, \gamma/v, E_{total}, K_{E}, F_{total}, K_{F}, Ca(t=0)\right)\), we can substitute these values in Eq. \ref{eq:FullModel}, solve it (numerically) and *predict* the time course of calcium at any positive time. Let us write \(Ca\left(t;\theta\right)\) this solution of Eq. \ref{eq:FullModel}. In addition to this model, we have using the results of the previous section, on a set of \(N\) time points \(t_i\), a collection of \(\left(\widehat{Ca}(t_i),\hat{\sigma}_{\widehat{Ca}(t_i)}\right)\). We can then define a first *objective* or *cost* function:

\begin{equation}\label{eq:cost1}
\Omega(\theta) \equiv \sum_{i=1}^N\left(\frac{\widehat{Ca}(t_i)-Ca\left(t_i;\theta\right)}{\hat{\sigma}_{\widehat{Ca}(t_i)}}\right)^2 \, .
\end{equation}  

Our problem then is to find the value of \(\widehat{\theta}\) that makes \(\Omega\) as small as possible, this a classical [non-linear weighted least squares](https://en.wikipedia.org/wiki/Non-linear_least_squares) problem (see H. B. Nielsen, K. Madsen, 2010, [Introduction to Optimization and Data Fitting](http://www2.imm.dtu.dk/pubdb/views/publication_details.php?id=5938)). The advantage of this setting is that if we can find \(\widehat{\theta}\) (that is, if the minimum exists and is unique), we also get:

1.  Confidence intervals for each element of \(\theta\) through their standard error (in fact more than that, since we get the full covariance matrix).
2.  The distribution from which \(\Omega(\widehat{\theta})\) should have been drawn if the model, \(Ca(t;\widehat{\theta})\), is indeed correct: a \(\chi^2\) distribution with \(N-\#\theta\) degrees of freedom, where \(\#\theta\) stands for the number of elements of \(\theta\) that are set from the fitted data.

*Point 2 gives us an objective basis to accept or reject a fit, an essential ingredient for the procedure presented here*.

Before going further, we should stop for a while and consider that \(\theta\) in the form just defined contains 7 elements among which, 2 are supposed to be known, \(K_F\) and \(F_{total}\). That leaves 5 values to extract from the transient. This is worrying since the latter seem, at first glance at least, close to a mono-exponential: \(Ca(t;Ca_{steady},\Delta,\tau) = Ca_{steady} + \Delta \, \exp (-t/\tau)\) (from which only 3 parameters can be obtained). We therefore follow Neher and Augustine (1992, p. 279) and consider an approximation of Eq. \ref{eq:FullModel} where \(Ca(t)\) can be written as \(Ca_{steady} + \delta Ca(t)\) and *where* \(\delta Ca(t)\) *is small enough for a first order expansion of* \(Ca(t)\) *in the vicinity of* \(Ca_{steady}\) *to be valid*. If we do this first order expansion, neglecting the communication between the pipette and the cytosol during the transient we get (setting \(\rho=0\) in Neher and Augustine, 1992, Eq. 15, p. 279):

\begin{equation}\label{eq:LinearModel}
\frac{\mathrm{d}\delta Ca(t)}{\mathrm{dt}} = - \frac{\gamma/v}{1+\kappa_{F}(Ca_{steady}) + \kappa_{E}(Ca_{steady})} \, \delta Ca(t) \, .
\end{equation}

Leading to:

\begin{equation}\label{eq:LinearModelSolution}
Ca(t;Ca_{steady},\Delta,\tau) = Ca_{steady} + \Delta \exp -t/\tau \, ,
\end{equation}

where the time constant \(\tau\) is defined by:

\begin{equation}\label{eq:tauVsKappa}
\tau(\kappa_{F}) \equiv \frac{1+\kappa_{F} + \kappa_{E}}{\gamma/v} \, ,
\end{equation}  

the dependence of the \(\kappa\) on \(Ca_{steady}\) has been made implicit. We see that if we can obtain several transients with different values of \([Fura]_{total}\) leading to several values of \(\kappa_{F}\), we can exploit the linear dependence of \(\tau\) on both  \(\kappa\) to estimate \(\kappa_E\) by extrapolation. This is the essence of the added buffer approach. The implementation of the latter requires:

1.  Get several (known) values of \([Fura]_{total}\) and therefore \(\kappa_{F}\) in the recorded neuron.
2.  Trigger a calcium entry at each of these \(\kappa_{F}\) values and fit *the tail of the transient* with a mono-exponential (Eq. \ref{eq:LinearModelSolution})&#x2013;fitting the tail ensures that the linear approximation of Eq. \ref{eq:FullModel} by Eq. \ref{eq:LinearModel} is valid&#x2013;.
3.  Plot \(\tau\) versus \(\kappa_F\) and fit a straight line to these points. The slope gives \(v/\gamma\) and the x-axis intercept gives \(\kappa_E\).

The fit in point 2 is done with weighted non-linear least squares using \(Ca(t;Ca_{steady},\Delta,\tau)\) (Eq. \ref{eq:LinearModelSolution}) instead of \(Ca(t;\theta)\) in the cost function definition (Eq. \ref{eq:cost1}). Each fits gives \(\hat{\tau}(\kappa_F)\) and \(\hat{\sigma}_{\hat{\tau}(\kappa_F)}\) *together* with a goodness of fit criterion. The \((\hat{\tau}(\kappa_F), \hat{\sigma}_{\hat{\tau}(\kappa_F)})\) are used in point 3 for a weighted *linear* least squares fit that also leads to confidence intervals on the estimated parameters and to a goodness of fit criterion.   


## Numerical details

The codes doing the "heavy" part of the analysis were written in `C`. Some higher level scripts were written in `Python 3`. The codes, their documentation, the data and the way the codes were applied to the data can be found on `GitHub` (<https://github.com/christophe-pouzat/hess-et-al-beta-escin-2019>) as well as on `zenodo` (I will add the address as soon as we make it public). The weighted linear and non-linear least squares routines as well as the random number generators are the ones of the Gnu Scientific Library ([GSL](http://www.gnu.org/software/gsl/)). The [Lenvenberg-Marquardt](http://www.gnu.org/software/gsl/doc/html/nls.html#levenberg-marquardt) implementation of the GSL was used for the non-linear least squares. Optimization was ended based on convergence criteria looking at the relative change of each estimated parameter value and at the gradient norm (see the [GSL documentation](http://www.gnu.org/software/gsl/doc/html/nls.html#testing-for-convergence)); as a safeguard a maximum number of iterations was set but all the optimizations did actually stop because the convergence criteria were met. 

The decaying part of each transient was fitted from the time at which half of the peak increase was lost (in order to make the linear approximation valid). This means that the number of data points used for each individual fit varies a bit from transient to transient. *This choice of fitting from the "half-peak time" was made before seeing the data*. It is important to realize that once such a choice has been made, we have no freedom anymore, the fit is done automatically and the residual sum of squares (RSS) is compatible or not with the theoretical \(\chi^2\) distribution.  

Since we used a triple wavelength (340, 380 *and* 360) protocol for each point of the transient, we have access to an independent estimate of \([Fura]_{total}\). We saw, as in Joucla et al (2010, Fig. 5, p. 1138), that the latter is not constant during the transients. When doing the \(\tau\) versus \(\kappa_F\) linear regression we therefore computed systematically three regressions using the minimum, the mean and the maximum observed values of \([Fura]_{total}\) during the transient. We always report the results of regression leading to the smallest RSS (best fit). As the reader can check from our `GitHub` repository none of our conclusions depend on this. We emphasize again that since our method generates standard errors for \(\hat{\tau}\), the linear regressions is accepted or rejected on an objective basis (the RSS is compatible or not with the relevant theoretical \(\chi^2\) distribution). 

