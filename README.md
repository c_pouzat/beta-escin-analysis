# β-escin analysis

This project contains `C` and `Python` codes, `makefiles` and `shell` scripts for the analysis of the 'β-escin' based perforated clamp data from _Subtantia nigra_ dopaminergic neurons as well as cerebellar Purkinje cells. 

This analysis is presented in the second half of [Analysis of neuronal Ca2+ handling properties by combining perforated patch clamp recordings and the added buffer approach](https://www.biorxiv.org/content/10.1101/2020.07.10.164202v1).

## Content

`beta_ecsin_supp_C.pdf` and `beta_ecsin_supp_C.html` contain the complete description and documentation of the `C` codes developed for this project. They should be your entry point. The code description is associated with examples of code use.

- `code` contains the `C` codes developed for this analysis as well as the `makefile` required for building the library and associated programs. The `Gnu Scientific Library` is heavily used and must be available.
- `data_paper` contain the experimental data analyzed in the manuscript in `HDF5` format. There are three sub-folders whose names indicate what cell type was recorded in what condition: `data_beta_escin` (SN dopaminergic neurons recorded with β-escin perforated clamp), `data_whole_cell` (SN dopaminergic neurons recorded with classical whol-cell) and `data_beta_escin_Purkinje` (cerebellar Purkinje cells).
- `analysis` contains some `Python` script running the analysis of the data contained in `data_paper` in an automatic way. The results of the analysis carried on the three sub-folders of `data_paper` are contained in the following archives: `analysis_DA_beta.tar.gz`, `analysis_DA_wc.tar.gz` and `analysis_PK.tar.gz`.
- `data` and `figs` are by products of the generation of `beta_ecsin_supp_C.pdf` and `beta_ecsin_supp_C.html`.


The data were recorded by Simon Hess in the laboratory of [Peter Kloppenburg](http://www.cecad.uni-koeln.de/index.php?id=82&type=0), Cologne Biocenter.
